#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# vim: ft=python fileencoding=utf-8 sts=4 sw=4 et:
"""Convert tsv files with intensifiers to a dataset."""

import csv
import gzip
import html
import json
import logging
import math
import numpy as np
import re
import string
import sys

from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser, FileType
from collections import Counter, OrderedDict
from pathlib import Path
from statistics import mean, stdev, variance
from typing import Dict, List, Union

__version__ = "0.1.0"


def to_categorical(
    classes: Union[str, List[str]], mapping: Dict[str, int]
) -> List[str]:
    """Convert a class or a list of classes with the given mapping to a one hot
    encoded list."""
    x = ["0"] * len(mapping.keys())
    if type(classes) == str:
        classes = [classes]
    for c in classes:
        x[mapping[c]] = "1"
        return x


def lemmatize(s: str) -> str:
    s = re.sub(r"a+h+", "ah", s)
    s = re.sub(r"a+lt", "alt", s)
    s = re.sub(r"a+w+", "aw", s)
    s = re.sub(r"bä+h+", "bäh", s)
    s = re.sub(r"blee+d", "bleed", s)
    s = re.sub(r"blö+d", "blöd", s)
    s = re.sub(r"bö+se", "böse", s)
    s = re.sub(r"bu+h+", "buh", s)
    s = re.sub(r"du+mm+", "dumm", s)
    s = re.sub(r"coo+l+", "cool", s)
    s = re.sub(r"cu+t+e+", "cute", s)
    s = re.sub(r"da+nke+", "danke", s)
    s = re.sub(r"doo+f", "doof", s)
    s = re.sub(r"du+nke+l", "dunkel", s)
    s = re.sub(r"dü+nn", "dünn", s)
    s = re.sub(r"fam+e", "fame", s)
    s = re.sub(r"frü+h+", "früh", s)
    s = re.sub(r"ge+i+lo+", "geil", s)
    s = re.sub(r"gespann+t", "gespannt", s)
    s = re.sub(r"goo+d", "good", s)
    s = re.sub(r"glückli+ch", "glücklich", s)
    s = re.sub(r"g+r+", "gr", s)
    s = re.sub(r"he+i+(s+|ß+)", "heiß", s)
    s = re.sub(r"hü+bsch+", "hübsch", s)
    s = re.sub(r"i+h+", "ih", s)
    s = re.sub(r"k+a+l+t+", "kalt", s)
    s = re.sub(r"kra+s", "krass", s)
    s = re.sub(r"krass+", "krass", s)
    s = re.sub(r"la+ng", "lang", s)
    s = re.sub(r"la+ngsa+m", "langsam", s)
    s = re.sub(r"la+n+g+w+e+i+l+i+g+", "langweilig", s)
    s = re.sub(r"lu+sti+g", "lustig", s)
    s = re.sub(r"li+e+b", "lieb", s)
    s = re.sub(r"lo+l", "lol", s)
    s = re.sub(r"mü+d+e+", "müde", s)
    s = re.sub(r"ne+", "ne", s)
    s = re.sub(r"ni+c+e+", "nice", s)
    s = re.sub(r"ni+e+dli+ch", "niedlich", s)
    s = re.sub(r"nö+", "nö", s)
    s = re.sub(r"o+h+", "oh", s)
    s = re.sub(r"omg+", "omg", s)
    s = re.sub(r"scha+de+", "schade", s)
    s = re.sub(r"s+(ü+|ue+|ü+hü+)(ß+|s+)?", "süß", s)
    s = re.sub(r"swee+t+", "sweet", s)
    s = re.sub(r"to+ll+", "toll", s)
    s = re.sub(r"vi+e+l+", "viel", s)
    s = re.sub(r"wa+rm+", "warm", s)
    s = re.sub(r"we+it", "weit", s)
    s = re.sub(r"wundervoll+", "wundervoll", s)
    s = re.sub(r"y+es+", "yes", s)
    return s


if __name__ == "__main__":
    parser = ArgumentParser(
        prog="intensifiers", formatter_class=ArgumentDefaultsHelpFormatter
    )
    parser.add_argument(
        "-V", "--version", action="version", version=f"%(prog)s v{__version__}"
    )
    parser.add_argument("-v", "--verbose", action="count", default=0)
    parser.add_argument(
        "-f",
        "--log-format",
        help="Logging format.",
        default="%(asctime)s %(levelname)s %(message)s",
    )
    parser.add_argument(
        "--validation-split",
        help="how much data to take for validation.",
        default=0.2,
    )
    parser.add_argument(
        "--seed",
        help="seed for the RNG.",
        default=123456789,
    )
    parser.add_argument(
        "--min-frequency",
        type=float,
        default=0.001,
        help="mininum symbol frequency.",
    )
    parser.add_argument(
        "--min-intensifier-frequency",
        type=int,
        default=75,
        help="mininum intensifier frequency.",
    )
    parser.add_argument(
        "--translate",
        action="store_true",
        help="translate tweets for train / validation sets.",
    )
    parser.add_argument(
        "--max-stack-size",
        type=int,
        default=-1,
        help="maximum stack size.",
    )
    parser.add_argument(
        "--predict-end-of-stack",
        action="store_true",
        help="add class to predict the end of a stack.",
    )
    parser.add_argument(
        "--min-symbols",
        type=str,
        default=string.ascii_letters + string.digits + string.punctuation + "äöüÄÖÜß",
        help="mininum symbols that should be present.",
    )
    parser.add_argument(
        "--blacklist",
        type=FileType("r", encoding="utf8"),
        default=None,
        help="intensifier blacklist to filter out.",
    )
    parser.add_argument(
        "--intensifiers",
        type=FileType("r", encoding="utf8"),
        default=None,
        help="intensifier list to filter for.",
    )
    parser.add_argument(
        "OUTPUT_DIR",
        type=lambda p: Path(p).absolute(),
        help="output training set.",
    )
    parser.add_argument(
        "TSVS",
        type=FileType("r", encoding="utf8"),
        nargs="+",
        default=sys.stdin,
        help="TSV files",
    )

    args = parser.parse_args()
    if args.verbose == 0:
        logging.basicConfig(format=args.log_format, level=logging.WARN)
    elif args.verbose == 1:
        logging.basicConfig(format=args.log_format, level=logging.INFO)
    else:
        logging.basicConfig(format=args.log_format, level=logging.DEBUG)

    rng = np.random.default_rng(args.seed)

    intensifier_filters = set()
    if args.intensifiers is not None:
        for line in args.intensifiers:
            intensifier_filters.add(line.strip())
        logging.info(
            f"Number of intensifiers to filter for: {len(intensifier_filters)}"
        )
    intensifier_blacklist = list()
    if args.blacklist is not None:
        for line in args.blacklist:
            intensifier_blacklist.append(line.strip())
        logging.info(
            f"Number of blacklisted intensifiers: {len(intensifier_blacklist)}"
        )

    adjds = Counter()
    data = list()
    intensifiers = Counter()
    stacks = dict()
    startpos_tofix = list()
    unkown_intensifiers = dict()
    stacks_counter = Counter()
    num_orig_tweets = 0
    num_startpos = 0
    num_wrong_startpos = 0
    tweets = set()
    removed_intensifiers = set()
    symbols = Counter()

    fields = [
        "VAFIN",
        "1",
        "2",
        "3",
        "4",
        "5",
        "6",
        "ADJD",
        "Startpos",
        "Endpos",
        "Sentence",
    ]

    for tsv in args.TSVS:
        logging.info(f"Parsing data from {tsv.name}.")
        reader = csv.DictReader(tsv, delimiter="\t")
        for row in reader:
            num_orig_tweets += 1

            tweet = re.sub(r"\s+", " ", row["Sentence"])
            tweet = re.sub(r"https?://[^\s]+", "", tweet)
            tweet = re.sub(r"^\s*(None|\d+)\s*\|", "", tweet).strip()
            tweet = html.unescape(tweet)

            c = []
            for i in ["1", "2", "3", "4", "5", "6"]:
                if row[i] == "0":
                    continue
                elif " " in row[i]:
                    logging.error(f"Blank in intensifier: {row[i]}")
                    sys.exit(1)
                elif row[i] in intensifier_blacklist:
                    c = []
                    break
                else:
                    c.append(row[i])
            if len(c) == 0:
                continue
            if args.max_stack_size > 0 and len(c) > args.max_stack_size:
                continue
            if args.intensifiers is not None:
                if any(i not in intensifier_filters for i in c):
                    removed_intensifiers.update(c)
                    if tweet not in tweets:
                        for i in c:
                            if i not in intensifier_filters:
                                if i not in unkown_intensifiers:
                                    unkown_intensifiers[i] = []
                                unkown_intensifiers[i].append(
                                    {
                                        "vafin": row["VAFIN"],
                                        "adjd": row["ADJD"],
                                        "tweet": tweet,
                                        "startpos": row["startpos"],
                                        "endpos": row["endpos"],
                                        "stack": " ".join(c),
                                    }
                                )
                    continue

            startpos = int(row["startpos"])
            endpos = int(row["endpos"])
            if len(row["Sentence"]) != len(tweet):
                if row["Sentence"][startpos:endpos] != tweet[startpos:endpos]:
                    s = re.sub(r"\s+", " ", row["Sentence"][startpos:endpos])
                    s = re.sub(r"https?://[^\s]+", "", s)
                    if tweet.find(row["Sentence"][startpos:endpos]) > 0:
                        endpos = endpos - (
                            startpos - tweet.find(row["Sentence"][startpos:endpos])
                        )
                        startpos = tweet.find(row["Sentence"][startpos:endpos])
                    else:
                        startpos = -1
                        endpos = -1
                        if tweet not in tweets:
                            startpos_tofix.append(
                                {
                                    "Startpos": startpos,
                                    "Endpos": endpos,
                                    "Sentence": tweet,
                                    "VAFIN": row["VAFIN"],
                                    "1": row["1"],
                                    "2": row["2"],
                                    "3": row["3"],
                                    "4": row["4"],
                                    "5": row["5"],
                                    "6": row["6"],
                                    "ADJD": row["ADJD"],
                                }
                            )
                        continue
            if startpos > 0 and tweet[startpos - 1] != " ":
                num_wrong_startpos += 1
                if tweet not in tweets:
                    startpos_tofix.append(
                        {
                            "Startpos": startpos,
                            "Endpos": endpos,
                            "Sentence": tweet,
                            "VAFIN": row["VAFIN"],
                            "1": row["1"],
                            "2": row["2"],
                            "3": row["3"],
                            "4": row["4"],
                            "5": row["5"],
                            "6": row["6"],
                            "ADJD": row["ADJD"],
                        }
                    )
                continue
            elif startpos == 0:
                test_startpos = tweet.find(row["VAFIN"]) + len(row["VAFIN"]) + 1
                s = f"{' '.join(c)} {row['ADJD']}"
                if tweet[test_startpos:endpos] == s:
                    startpos = test_startpos
                elif tweet[test_startpos:endpos].endswith(f" {s}"):
                    startpos = tweet.find(s, test_startpos, endpos)
                else:
                    num_startpos += 1
                    if tweet not in tweets:
                        startpos_tofix.append(
                            {
                                "Startpos": startpos,
                                "Endpos": endpos,
                                "Sentence": tweet,
                                "VAFIN": row["VAFIN"],
                                "1": row["1"],
                                "2": row["2"],
                                "3": row["3"],
                                "4": row["4"],
                                "5": row["5"],
                                "6": row["6"],
                                "ADJD": row["ADJD"],
                            }
                        )
                    continue

            if tweet in tweets:
                continue
            else:
                tweets.add(tweet)
                symbols.update(tweet)

            row["ADJD"] = lemmatize(row["ADJD"])
            if row["ADJD"] not in stacks:
                stacks[row["ADJD"]] = 0

            adjds.update([row["ADJD"]])
            stacks_counter.update([" ".join(c + [row["ADJD"]])])
            intensifiers.update(c)

            stacks[row["ADJD"]] += 1
            data.append(
                {
                    "stack": c,
                    "Startpos": startpos,
                    "Endpos": endpos,
                    "Sentence": tweet,
                    "VAFIN": row["VAFIN"],
                    "1": row["1"],
                    "2": row["2"],
                    "3": row["3"],
                    "4": row["4"],
                    "5": row["5"],
                    "6": row["6"],
                    "ADJD": row["ADJD"],
                }
            )
    removed_intensifiers.difference_update(intensifiers)

    logging.info(f"Tweets: {num_orig_tweets}")
    logging.info(f"Tweets with wrong startpos: {num_wrong_startpos}")
    logging.info(f"Tweets with startpos 0: {num_startpos}")
    logging.info(f"Tweets after cleaning {len(tweets)}")

    logging.info(f"ADJDs: {len(adjds)}")
    logging.info(f"Intensifiers: {len(intensifiers)}")
    logging.info(f"Words: {len(set(list(adjds.keys()) + list(intensifiers.keys())))}")
    logging.info(f"Stacks: {len(stacks_counter)}")

    intensifiers = dict(sorted(intensifiers.items(), key=lambda x: x[1]))

    if args.predict_end_of_stack:
        if args.max_stack_size == 1:
            intensifier_mappings = dict()
        else:
            intensifier_mappings = {"$": 0}
    else:
        intensifier_mappings = dict()

    for word, freq in intensifiers.items():
        if freq < args.min_intensifier_frequency:
            continue
        intensifier_mappings[word] = len(intensifier_mappings)

    stacks_data = dict((i, list()) for i in range(1, 6))
    to_del = []
    for i in range(len(data)):
        if len(data[i]["stack"]) not in stacks_data.keys():
            stacks_data[len(data[i]["stack"])] = []
        if any(j not in intensifier_mappings for j in data[i]["stack"]):
            adjds.subtract([data[i]["ADJD"]])
            stacks_counter.subtract([" ".join(data[i]["stack"] + [data[i]["ADJD"]])])
            to_del.append(data[i])
            continue
        stacks_data[len(data[i]["stack"])].append(
            {
                "stack": data[i]["stack"],
                "adjd": data[i]["ADJD"],
                "startpos": data[i]["Startpos"],
                "endpos": data[i]["Endpos"],
                "tweet": data[i]["Sentence"],
            }
        )
        del data[i]["stack"]
    for i in to_del:
        tweets.remove(i["Sentence"])
        data.remove(i)
    logging.info(f"ADJDs after filtering: {len(adjds)}")
    logging.info(f"Intensifiers after filtering: {len(intensifier_mappings) - 1}")
    logging.info(
        f"Words after filtering: {len(set(list(adjds.keys()) + list(intensifiers.keys())))}"
    )
    logging.info(f"Stacks after filtering: {len(stacks_counter)}")
    logging.info(f"Tweets after filtering: {len(tweets)}")

    nb_val = dict(
        (k, math.floor(len(v) * args.validation_split))
        for k, v in sorted(stacks_data.items(), key=lambda x: x[0])
    )
    for k, v in sorted(stacks_data.items(), key=lambda x: x[0]):
        logging.info(f"  * {k}: {len(v)} ({nb_val[k]})")

    min_freq = (symbols.total() - symbols[" "]) * args.min_frequency
    min_symbols = set(args.min_symbols)

    vocab = OrderedDict()
    vocab_data = OrderedDict()
    for i in sorted(min_symbols):
        vocab[i] = len(vocab) + 2
        vocab_data[i] = {"id": vocab[i], "frequency": symbols[i]}
    for k, v in symbols.items():
        if v >= min_freq and k not in vocab:
            vocab[k] = len(vocab) + 2
            vocab_data[k] = {"id": vocab[k], "frequency": v}

    stack_statistics = dict()
    for adjd, freq in adjds.items():
        if freq == 0:
            continue
        if adjd not in stack_statistics:
            stack_statistics[adjd] = 0
        stack_statistics[adjd] = stacks[adjd]
    for k, v in stack_statistics.items():
        adjds[k] = {"frequency": adjds[k], "num_stacks": v}

    stack_mean = mean(stack_statistics.values())
    logging.info("Stack statistics per ADJD:")
    logging.info(f"  * mean: {stack_mean}")
    logging.info(f"  * max: {max(stack_statistics.values())}")
    logging.info(f"  * variance: {variance(stack_statistics.values(), stack_mean)}")
    logging.info(f"  * stdev: {stdev(stack_statistics.values(), stack_mean)}")

    with gzip.open(args.OUTPUT_DIR / "data.csv.gz", "wt", encoding="utf8") as f:
        writer = csv.DictWriter(f, fields, dialect="unix")
        writer.writeheader()
        writer.writerows(data)

    with gzip.open(
        args.OUTPUT_DIR / "wrong_startpos.csv.gz", "wt", encoding="utf8"
    ) as f:
        writer = csv.DictWriter(f, fields, dialect="unix")
        writer.writeheader()
        writer.writerows(startpos_tofix)

    logging.info("Creating training and validation sets.")
    logging.info("Saving train data to train.csv.gz and val data to val.csv.gz.")
    with gzip.open(
        args.OUTPUT_DIR / "train.csv.gz", "wt", encoding="utf8"
    ) as ftrain, gzip.open(
        args.OUTPUT_DIR / "val.csv.gz", "wt", encoding="utf8"
    ) as fval:
        train_writer = csv.DictWriter(
            ftrain,
            ["id", "tweet", "adjd", "stack", "next", "direction"],
            dialect="unix",
        )
        train_writer.writeheader()

        val_writer = csv.DictWriter(
            fval, ["id", "tweet", "adjd", "stack", "next", "direction"], dialect="unix"
        )
        val_writer.writeheader()

        num_train = 0
        num_val = 0
        for k, data in stacks_data.items():
            for i in rng.permutation(len(data)):
                startpos = data[i]["startpos"]
                endpos = data[i]["endpos"]

                rows = []
                for j in range(len(data[i]["stack"]) + 1):
                    if j == len(data[i]["stack"]):
                        if args.predict_end_of_stack:
                            intensifier_stack = data[i]["stack"]
                            next_intensifier = "$"
                            tweet = data[i]["tweet"]
                    else:
                        intensifier_stack = [] if j == 0 else data[i]["stack"][:j]
                        next_intensifier = data[i]["stack"][j]
                        tweet = (
                            data[i]["tweet"][:startpos]
                            + " ".join(
                                data[i]["tweet"][startpos:endpos].rsplit(
                                    maxsplit=j + 1
                                )[1:]
                            )
                            + data[i]["tweet"][endpos:]
                        )
                    rows.append((intensifier_stack, next_intensifier, tweet, 1))
                for j in range(len(data[i]["stack"]), -1, -1):
                    if j == len(data[i]["stack"]):
                        if args.predict_end_of_stack:
                            intensifier_stack = data[i]["stack"]
                            next_intensifier = "$"
                            tweet = data[i]["tweet"]
                    else:
                        intensifier_stack = [] if j == 0 else data[i]["stack"][j:]
                        next_intensifier = data[i]["stack"][j - 1]
                        tweet = (
                            data[i]["tweet"][:startpos]
                            + " ".join(
                                data[i]["tweet"][startpos:endpos].rsplit(
                                    maxsplit=j + 1
                                )[1:]
                            )
                            + data[i]["tweet"][endpos:]
                        )
                    rows.append((intensifier_stack, next_intensifier, tweet, -1))

                for j, (
                    intensifier_stack,
                    next_intensifier,
                    tweet,
                    direction,
                ) in enumerate(rows):
                    if args.translate:
                        tweet = ",".join(
                            [
                                str(vocab[s] if s in vocab else (len(vocab.keys()) + 2))
                                for s in tweet
                            ]
                        )

                    if i >= nb_val[len(data[i]["stack"])]:
                        num_train += 1
                        train_writer.writerow(
                            {
                                "id": f"train-{k}.{i}-{j}",
                                "adjd": data[i]["adjd"],
                                "tweet": tweet,
                                "stack": ",".join(intensifier_stack),
                                "next": intensifier_mappings[next_intensifier],
                                "direction": direction,
                            }
                        )
                    else:
                        num_val += 1
                        val_writer.writerow(
                            {
                                "id": f"val-{k}.{i}-{j}",
                                "adjd": data[i]["adjd"],
                                "tweet": tweet,
                                "stack": ",".join(intensifier_stack),
                                "next": intensifier_mappings[next_intensifier],
                                "direction": direction,
                            }
                        )
        logging.info(f"  * num train: {num_train}")
        logging.info(f"  * num val: {num_val}")

    logging.info("Saving vocab to vocab.json.")
    with open(args.OUTPUT_DIR / "vocab.json", "w", encoding="utf8") as f:
        f.write(json.dumps(vocab_data, ensure_ascii=False, indent=4))
        f.write("\n")

    logging.info("Saving mappings to mappings.json.")
    with open(args.OUTPUT_DIR / "mappings.json", "w", encoding="utf8") as f:
        f.write(
            json.dumps(
                {
                    "intensifier": dict(
                        (
                            k,
                            {
                                "id": v,
                                "frequency": None if k == "$" else intensifiers[k],
                            },
                        )
                        for k, v in intensifier_mappings.items()
                    ),
                    "adjds": adjds,
                },
                ensure_ascii=False,
                indent=4,
            )
        )
        f.write("\n")

    logging.info("Saving tweets with unkown intensifiers unkown.json.gz.")
    with gzip.open(args.OUTPUT_DIR / "unkown.json.gz", "wt", encoding="utf8") as f:
        f.write(
            json.dumps(
                unkown_intensifiers,
                ensure_ascii=False,
                indent=4,
            )
        )
        f.write("\n")
